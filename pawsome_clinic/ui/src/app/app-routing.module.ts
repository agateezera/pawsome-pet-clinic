import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {AuthGuardService} from "./service/auth-guard.service";
import {RegisterComponent} from "./register/register.component";
import {HomeComponent} from "./home/home.component";
import {UserListComponent} from "./users/user-list/user-list.component";
import {UpdateUserComponent} from "./users/update-user/update-user.component";
import {CreateUserComponent} from "./users/create-user/create-user.component";
import {UserDetailsComponent} from "./users/user-details/user-details.component";
import {PetListComponent} from "./pets/pet-list/pet-list.component";
import {CreatePetComponent} from "./pets/create-pet/create-pet.component";
import {UpdatePetComponent} from "./pets/update-pet/update-pet.component";
import {PetDetailsComponent} from "./pets/pet-details/pet-details.component";
import {UserProfileComponent} from "./user-profile/user-profile.component";
import {UserPetListComponent} from "./user-pet-list/user-pet-list.component";
import {ProcedureComponent} from "./procedures/procedure.component";
import {CreateAppointmentComponent} from "./appointments/create-appointment/create-appointment.component";
import {AppointmentListComponent} from "./appointments/appointment-list/appointment-list.component";
import {UpdateAppointmentComponent} from "./appointments/update-appointment/update-appointment.component";
import {CreateRecordComponent} from "./medical-records/create-record/create-record.component";
import {UpdateRecordComponent} from "./medical-records/update-record/update-record.component";
import {RecordListComponent} from "./medical-records/record-list/record-list.component";

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent},
  { path: 'logout', component: LoginComponent},
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: HomeComponent},
  { path: 'services', component: ProcedureComponent},
  { path: 'profile', component: UserProfileComponent, canActivate: [AuthGuardService]},
  { path: 'users', component: UserListComponent, canActivate: [AuthGuardService] },
  { path: 'add', component: CreateUserComponent, canActivate: [AuthGuardService] },
  { path: 'update/:id', component: UpdateUserComponent, canActivate: [AuthGuardService] },
  { path: 'details/:id', component: UserDetailsComponent, canActivate: [AuthGuardService] },
  { path: 'pets', component: PetListComponent, canActivate: [AuthGuardService] },
  { path: 'add-pet', component: CreatePetComponent , canActivate: [AuthGuardService]},
  { path: 'user/:id/add-pet', component: CreatePetComponent , canActivate: [AuthGuardService]},
  { path: 'update-pet/:id', component: UpdatePetComponent , canActivate: [AuthGuardService]},
  { path: 'pet-details/:id', component: PetDetailsComponent, canActivate: [AuthGuardService]},
  { path: 'user/:id/pets', component: UserPetListComponent, canActivate: [AuthGuardService]},
  { path: 'medical-records/create', component: CreateRecordComponent, canActivate: [AuthGuardService]},
  { path: 'medical-records/update', component: UpdateRecordComponent, canActivate: [AuthGuardService]},
  { path: 'medical-records', component: RecordListComponent, canActivate: [AuthGuardService]},
  { path: 'user/:id/add-appointment', component: CreateAppointmentComponent, canActivate: [AuthGuardService]},
  { path: 'appointments/update', component: UpdateAppointmentComponent, canActivate: [AuthGuardService]},
  { path: 'appointments', component: AppointmentListComponent, canActivate: [AuthGuardService]},

];


@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: "reload"
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
