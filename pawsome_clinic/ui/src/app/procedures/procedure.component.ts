import { Component, OnInit } from '@angular/core';
import {ProcedureService} from './procedure.service';
import {NavigationEnd, Router, RouterEvent} from "@angular/router";
import {filter} from "rxjs/operators";
import {Observable, Subject} from "rxjs";
// @ts-ignore
import {Procedure} from "../models/procedure";

@Component({
  selector: 'app-procedures',
  templateUrl: './procedure.component.html',
  styleUrls: ['./procedure.component.css']
})
export class ProcedureComponent implements OnInit {

  public destroyed = new Subject<any>();
  procedures: Observable<Procedure[]>;

  constructor(private procedureService: ProcedureService,
              private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.procedures = this.procedureService.getProcedureList();
  }

  ngOnDestroy(): void {
    this.destroyed.next();
    this.destroyed.complete();

  }

}
