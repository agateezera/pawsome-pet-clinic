export class Procedure {
  id: number;
  durationInMin: number;
  price: number;
  name: string;
}
