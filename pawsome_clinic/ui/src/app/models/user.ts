export class User {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  password:string;
  email:string;
  phone:string;
  enabled: boolean;
  fileName: string;
  roles: [];
  pets: [];
  appointments: [];
}
