import { Component, OnInit } from '@angular/core';
import {User} from "../../models/user";
import { UserService } from '../user.service';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  id: number;
  user: User;

  constructor(private route: ActivatedRoute,private router: Router,
              private userService: UserService) { }

  ngOnInit() {
    this.user = new User();

    this.id = this.route.snapshot.params['id'];

    this.userService.getUser(this.id)
      .subscribe(data => {
        console.log(data)
        this.user = data;
      }, error => console.log(error));
  }

  updateUser() {
    this.userService.updateUser(this.id, this.user)
      .subscribe(data => {
        console.log(data);
        this.user = new User();
        this.gotoList();
      }, error => console.log(error));
  }

  onSubmit() {
    this.updateUser();
  }

  gotoList() {
    this.router.navigate(['/users']);
  }

  // id: string;
  // user: User;
  //
  // constructor(private userService: UserService,
  //             private route: ActivatedRoute,
  //             private router: Router) {
  //   this.userService.findById(this.route.snapshot.paramMap.get('id'))
  //     .subscribe(
  //       (data) => {
  //         this.user = data;
  //       }
  //     );
  // }
  //
  // onSubmit(){
  //   this.userService
  //     .save(this.user)
  //     .subscribe(() => {
  //       this.router.navigate(['/users']);
  //     });
  // }
  //
  // ngOnInit(): void {
  //   this.id = this.route.snapshot.paramMap.get('id');
  // }

}
