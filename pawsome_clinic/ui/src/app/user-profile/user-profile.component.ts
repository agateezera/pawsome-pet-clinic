import { Component, OnInit } from '@angular/core';
import { UserService } from '../users/user.service';
import {User} from "../models/user";
import {AuthenticationService} from "../service/authentication.service";
import {Router} from "@angular/router";
import {Pet} from "../models/pet";
import {PetService} from "../pets/pet.service";
import {Appointment} from "../models/appointment";
import {AppointmentService} from "../appointments/appointment.service";


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  user: User;
  pets: Array<Pet>;
  appointments: Array<Appointment>;


  username: string;

  constructor(private userService: UserService,
              private petService: PetService,
              private appointmentService: AppointmentService,
              private authenticationService: AuthenticationService,
              private router:Router) { }

  ngOnInit(): void {
    this.user = new User();

    this.username = this.authenticationService.getLoggedInUserName();
    this.reloadData();
  }

  reloadData() {
    this.userService.getUserByUsername(this.username)
      .subscribe(data => {
        console.log(data)
        this.user = data;
        this.pets = this.user.pets;
        this.appointments = this.user.appointments;
      }, error => console.log(error));
  }

  onFileChanged(event) {
    const file = event.target.files[0]
  }

  onUpload() {
    // upload code goes here
  }

  deleteAppointment(id: number) {
    this.appointmentService.deleteAppointment(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

}
