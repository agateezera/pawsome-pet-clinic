package com.pawsomepetclinic.repository;

import com.pawsomepetclinic.model.Procedure;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcedureRepository extends JpaRepository<Procedure, Long> {
}
