package com.pawsomepetclinic.repository;

import com.pawsomepetclinic.model.pets.Breed;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BreedRepository extends JpaRepository<Breed, Long> {
}
