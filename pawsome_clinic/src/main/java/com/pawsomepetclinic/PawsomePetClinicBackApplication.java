package com.pawsomepetclinic;

import com.pawsomepetclinic.repository.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
public class PawsomePetClinicBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(PawsomePetClinicBackApplication.class, args);
    }

}
