package com.pawsomepetclinic.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AppointmentDto {
    Long id;
    private String appointmentDate;
    private String appointmentTime;
    private Long userId;
    private String petName;
//    private Long petId;
    private Long procedureId;
}
