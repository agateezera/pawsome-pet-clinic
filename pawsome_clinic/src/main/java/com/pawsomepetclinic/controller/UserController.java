package com.pawsomepetclinic.controller;

import com.pawsomepetclinic.exception.ResourceNotFoundException;
import com.pawsomepetclinic.model.persons.User;
import com.pawsomepetclinic.model.pets.Pet;
import com.pawsomepetclinic.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping({ "/users" })
public class UserController {

    UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping()
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        return userService.getUserById(userId);
    }

    @PostMapping("/register")
    public User registerUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @PostMapping("")
    public User createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @PutMapping("/{id}")
    public ResponseEntity<User> updateUser(@PathVariable(value = "id") Long userId,
                                                   @RequestBody User userDetails) throws ResourceNotFoundException {
        return userService.updateUser(userId, userDetails);
    }

    @PutMapping("/profile/save-photo")
    public User saveProfilePhoto(User user, MultipartFile file) throws IOException {
        return userService.saveProfilePhoto(user, file);
    }

    @PutMapping("/profile/delete-photo")
    public User deleteProfilePhoto(User user) throws IOException {
        return userService.deleteProfilePhoto(user);
    }

    @PutMapping("/profile/update-password")
    public User updateUserPassword(User user, String password) throws IOException {
        return userService.updateUserPassword(user, password);
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        return userService.deleteUser(userId);
    }

    @GetMapping("/current")
    public ResponseEntity<String> getCurrentUser() {
        return ResponseEntity.ok("{ \"value\": \"" + userService.getCurrentUser() + "\"}");
    }

    @GetMapping("/checkAdmin")
    public boolean checkForAdmin(){
        return userService.checkForAdmin();
    }

    @GetMapping("/profile/{username}")
    public ResponseEntity<User> getCurrentUserDetails(@PathVariable(value = "username") String username) {
        return ResponseEntity.ok(userService.getCurrentUserDetails(username));
    }

}
