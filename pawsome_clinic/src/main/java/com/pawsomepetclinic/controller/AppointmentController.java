package com.pawsomepetclinic.controller;

import com.pawsomepetclinic.dto.AppointmentDto;
import com.pawsomepetclinic.exception.ResourceNotFoundException;
import com.pawsomepetclinic.model.Appointment;
import com.pawsomepetclinic.model.pets.Pet;
import com.pawsomepetclinic.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping({ "/appointments" })
public class AppointmentController {

    private AppointmentService appointmentService;

    @Autowired
    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @GetMapping("")
    public List<Appointment> getAllAppointments() {
        return appointmentService.getAllAppointments();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Appointment> getAppointmentById(@PathVariable(value = "id") Long appointmentId)
            throws ResourceNotFoundException {
        return appointmentService.getAppointmentById(appointmentId);
    }

    @PostMapping
    @Transactional
    public ResponseEntity<Void> createAppointment(@RequestBody AppointmentDto appointmentDto) throws ResourceNotFoundException {
        appointmentService.save(appointmentDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Appointment> updateAppointment(@PathVariable(value = "id") Long appointmentId,
                                         @RequestBody Appointment appointmentDetails) throws ResourceNotFoundException {
        return appointmentService.updateAppointment(appointmentId, appointmentDetails);
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteAppointment(@PathVariable(value = "id") Long appointmentId)
            throws ResourceNotFoundException {
        return appointmentService.deleteAppointment(appointmentId);
    }
}
